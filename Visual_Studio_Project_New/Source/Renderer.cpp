#include "Renderer.h"
#include "GeometryNode.h"
#include "Tools.h"
#include "ShaderProgram.h"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "OBJLoader.h"

#include <algorithm>
#include <array>
#include <iostream>
#include <math.h> 

// RENDERER
Renderer::Renderer()
{
	this->m_nodes = {};
	this->m_collidables_nodes = {};
	this->m_continous_time = 0.0;
	this->sum_x = 0.0;
	this->sum_y = 0.0;
	this->last_position_first_walls = 0.0;
	this->last_position_wall = 0.0;
	this->check_if_walls_opening = false;
	//this->sphere_position[3] = { 0.f, 0.f, 0.f, };
	this->sphere_position[0] = 0.f;
	this->sphere_position[1] = 0.f;
	this->sphere_position[2] = 0.f;
	this->sphere_released = false;
	this->sphere_initial_position[0] = glm::vec3(0.f);
	this->sphere_initial_position[1] = glm::vec3(0.f);
	this->sphere_initial_position[2] = glm::vec3(0.f);
	this->sphere_initial_target_position[0] = glm::vec3(0.f);
	this->sphere_initial_target_position[1] = glm::vec3(0.f);
	this->sphere_initial_target_position[2] = glm::vec3(0.f);
	this->spheres_gunner_array[0] = false;
	this->spheres_gunner_array[1] = false;
	this->spheres_gunner_array[2] = false;
	this->spheres_update_theta_array[0] = false;
	this->spheres_update_theta_array[1] = false;
	this->spheres_update_theta_array[2] = false;
	this->spheres_theta_y_array[0] = 0.f;
	this->spheres_theta_y_array[1] = 0.f;
	this->spheres_theta_y_array[2] = 0.f;
	this->spheres_theta_x_array[0] = 0.f;
	this->spheres_theta_x_array[1] = 0.f;
	this->spheres_theta_x_array[2] = 0.f;
}

Renderer::~Renderer()
{
	glDeleteTextures(1, &m_fbo_depth_texture);
	glDeleteTextures(1, &m_fbo_pos_texture);
	glDeleteTextures(1, &m_fbo_normal_texture);
	glDeleteTextures(1, &m_fbo_albedo_texture);
	glDeleteTextures(1, &m_fbo_mask_texture);

	glDeleteFramebuffers(1, &m_fbo);

	glDeleteVertexArrays(1, &m_vao_fbo);
	glDeleteBuffers(1, &m_vbo_fbo_vertices);
}

bool Renderer::Init(int SCREEN_WIDTH, int SCREEN_HEIGHT)
{
	this->m_screen_width = SCREEN_WIDTH;
	this->m_screen_height = SCREEN_HEIGHT;

	bool techniques_initialization = InitShaders();
	bool meshes_initialization = InitGeometricMeshes();
	bool light_initialization = InitLights();
	bool common_initialization = InitCommonItems();
	bool inter_buffers_initialization = InitIntermediateBuffers();

	//If there was any errors
	if (Tools::CheckGLError() != GL_NO_ERROR)
	{
		printf("Exiting with error at Renderer::Init\n");
		return false;
	}
	this->BuildWorld();
	this->InitCamera();
	std::cout<< techniques_initialization << meshes_initialization << light_initialization << inter_buffers_initialization  << std::endl;
	//If everything initialized
	return techniques_initialization && meshes_initialization && common_initialization && inter_buffers_initialization;
}

void Renderer::BuildWorld() {

	//CollidableNode& beam = *this->m_collidables_nodes[0];

	//FIRST_PART_DECLARATIONS
	CollidableNode& corridor_straight_1 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_1];
	GeometryNode& pipe_1 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_1];
	GeometryNode& pipe_2 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_2];
	CollidableNode& wall_1 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_1];
	CollidableNode& wall_2 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_2];
	CollidableNode& corridor_straight_2 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_2];
	GeometryNode& pipe_3 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_3];
	GeometryNode& pipe_4 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_4];
	GeometryNode& pipe_5 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_5];
	CollidableNode& corridor_fork_1 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_FORK_1];
	CollidableNode& wall_3 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_3];
	GeometryNode& cannon_mount_1 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_1];
	GeometryNode& cannon_mount_2 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_2];
	GeometryNode& cannon_1 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_1];
	GeometryNode& cannon_2 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_2];
	CollidableNode& beam_1 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::BEAM_1];

	CollidableNode& corridor_right_1 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_1];
	CollidableNode& corridor_straight_3 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_3];
	CollidableNode& wall_4 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_4];
	GeometryNode& cannon_mount_3 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_3];
	GeometryNode& cannon_3 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_3];
	GeometryNode& cannon_4 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_4];
	GeometryNode& iris_1 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_1];
	CollidableNode& wall_5 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_5];
	GeometryNode& iris_2 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_2];
	GeometryNode& iris_3 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_3];
	CollidableNode& corridor_straight_5 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_5];
	GeometryNode& pipe_6 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_6];
	GeometryNode& pipe_7 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_7];
	CollidableNode& beam_2 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::BEAM_2];
	CollidableNode& corridor_straight_6 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_6];
	GeometryNode& iris_7 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_7];
	GeometryNode& pipe_8 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_8];
	GeometryNode& pipe_9 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_9];
	GeometryNode& pipe_10 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_10];
	GeometryNode& pipe_11 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_11];

	CollidableNode& corridor_right_2 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_2];
	CollidableNode& corridor_straight_4 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_4];
	CollidableNode& wall_6 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_6];
	GeometryNode& cannon_mount_4 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_4];
	GeometryNode& cannon_5 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_5];
	GeometryNode& cannon_6 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_6];
	GeometryNode& iris_4 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_4];
	CollidableNode& wall_7 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_7];
	GeometryNode& iris_5 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_5];
	GeometryNode& iris_6 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_6];
	CollidableNode& corridor_fork_2 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_FORK_2];
	CollidableNode& beam_3 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::BEAM_3];
	CollidableNode& corridor_right_3 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_3];

	GeometryNode& sphere_1 = *this->m_nodes[GEOMETRY_OBJECTS::SPHERE_1];
	GeometryNode& sphere_2 = *this->m_nodes[GEOMETRY_OBJECTS::SPHERE_2];
	GeometryNode& sphere_3 = *this->m_nodes[GEOMETRY_OBJECTS::SPHERE_3];


	//FIRST_PART_TRANSFORMATIONS
	pipe_1.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 3.5f, 0.f));
	pipe_2.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(10.f, 3.5f, -10.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f));
	wall_1.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 0.f, -20.f));
	wall_2.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, 0.f, -20.f));
	corridor_straight_2.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -20.f));
	pipe_3.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(12.f, -3.5f, -30.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f));
	pipe_4.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(2.f, -3.5f, -30.f));
	pipe_5.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(2.f, -3.5f, -40.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f));
	corridor_fork_1.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -40.f));
	wall_3.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -3.f, -45.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(1.f, 0.f, 0.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));
	cannon_mount_1.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-3.f, -3.f, -45.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(1.f, 0.f, 0.f));
	cannon_mount_2.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, -3.f, -45.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(1.f, 0.f, 0.f));
	cannon_1.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-2.2f, -1.65f, -45.f));
	cannon_1.m_aabb.center = glm::vec3(cannon_1.model_matrix * glm::vec4(cannon_1.m_aabb.center, 1.f));
	cannon_2.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(2.2f, -1.65f, -45.f));
	beam_1.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -59.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));


	corridor_right_1.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, 0.f, -60.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f));
	corridor_straight_3.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, -5.f, -80.f));
	wall_4.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, -5.f, -90.f));
	cannon_mount_3.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, -2.f, -90.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f));
	cannon_3.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, -1.2f, -88.5f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));
	cannon_4.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, -2.8f, -88.5f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));
	iris_1.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, -8.f, -90.f));
	wall_5.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(7.f, -5.f, -100.f));
	iris_2.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(7.f, -2.f, -100.f));
	iris_3.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(7.f, -8.f, -100.f));
	corridor_straight_5.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, -5.f, -100.f));
	pipe_6.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(6.5f, -1.f, -110.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f));
	pipe_7.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(6.5f, -1.f, -110.f));
	beam_2.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, -5.f, -120.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(-45.f), glm::vec3(0.f, 0.f, 1.f));
	corridor_straight_6.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, -5.f, -120.f));
	iris_7.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, -10.2f, -135.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(1.f, 0.f, 0.f));
	pipe_8.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(6.5f, -1.f, -120.f));
	pipe_9.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(6.5f, -1.f, -130.f));
	pipe_10.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(6.5f, -1.f, -140.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f));
	pipe_11.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-3.5f, -1.f, -140.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f));


	corridor_right_2.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 0.f, -60.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));
	corridor_straight_4.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 5.f, -80.f));
	wall_6.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-7.f, 5.f, -90.f));
	cannon_mount_4.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-7.f, 8.f, -90.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f));
	cannon_5.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-7.f, 8.8f, -88.5f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));
	cannon_6.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-7.f, 7.2f, -88.5f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));
	iris_4.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-7.f, 2.f, -90.f));
	wall_7.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-3.f, 5.f, -100.f));
	iris_5.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-3.f, 8.f, -100.f));
	iris_6.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-3.f, 2.f, -100.f));
	corridor_fork_2.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 5.f, -100.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));
	beam_3.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 5.f, -120.f));
	corridor_right_3.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 0.f, -120.f)) *
		glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f));

	//SECOND_PART_DECLARATIONS
	CollidableNode& corridor_fork_3 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_FORK_3];
	CollidableNode& corridor_right_5 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_5];
	CollidableNode& wall_11 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_11];
	CollidableNode& wall_12 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_12];
	CollidableNode& beam_4 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::BEAM_4];

	CollidableNode& corridor_straight_7 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_7];
	CollidableNode& wall_8 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_8];
	CollidableNode& wall_9 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_9];
	CollidableNode& corridor_right_4 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_4];
	CollidableNode& wall_10 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_10];
	GeometryNode& iris_8 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_8];
	GeometryNode& iris_9 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_9];
	GeometryNode& pipe_12 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_12];
	GeometryNode& pipe_13 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_13];
	GeometryNode& pipe_14 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_14];
	GeometryNode& pipe_15 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_15];
	CollidableNode& corridor_straight_8 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_8];
	CollidableNode& wall_13 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_13];
	GeometryNode& iris_10 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_10];
	GeometryNode& cannon_7 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_7];
	GeometryNode& cannon_8 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_8];
	GeometryNode& cannon_mount_5 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_5];
	CollidableNode& corridor_fork_4 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_FORK_4];

	CollidableNode& corridor_straight_9 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_9];
	CollidableNode& corridor_right_6 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_6];
	GeometryNode& pipe_16 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_16];
	GeometryNode& pipe_17 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_17];
	CollidableNode& beam_5 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::BEAM_5];
	CollidableNode& corridor_straight_10 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_10];
	GeometryNode& pipe_18 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_18];
	GeometryNode& pipe_19 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_19];
	CollidableNode& wall_14 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_14];
	CollidableNode& wall_15 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_15];


	//SECOND_PART_TRANSFORMATIONS
	corridor_fork_3.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -5.f, -160.f)) *
								   glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(0.f, 1.f, 0.f));
	corridor_right_5.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -5.f, -160.f)) *
									glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));
	wall_11.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -160.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f));
	wall_12.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -10.f, -160.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f));
	beam_4.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -3.f, -180.f)) *
						  glm::rotate(glm::mat4(1.f), glm::radians(15.f), glm::vec3(1.f, 0.f, 0.f));

	corridor_straight_7.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 10.f, -120.f));
	wall_8.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-10.f, 10.f, -130.f));
	wall_9.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 10.f, -130.f));
	corridor_right_4.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 10.f, -140.f));
	////////////////////////////////////////////////////////////////////////////////////////////////////
	wall_10.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-4.1f, 7.2f, -150.5f)) *
						   //glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f)) *
						   //glm::rotate(glm::mat4(1.f), glm::radians(110.f), glm::vec3(0.f, 1.f, 0.f)) *
						   //glm::rotate(glm::mat4(1.f), glm::radians(55.f), glm::vec3(1.f, 0.f, 0.f));
						   //glm::rotate(glm::mat4(1.f), glm::radians(-20.f), glm::vec3(0.f, 0.f, 1.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(-13.f), glm::vec3(0.f, 1.f, 0.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(-30.f), glm::vec3(0.f, 0.f, 1.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(1.f, 0.f, 0.f));
	iris_8.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-4.8f, 7.1f, -147.6f)) *
						  glm::rotate(glm::mat4(1.f), glm::radians(60.f), glm::vec3(0.f, 0.f, 1.f)) *
						  glm::rotate(glm::mat4(1.f), glm::radians(77.f), glm::vec3(0.f, 1.f, 0.f));
	iris_9.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 6.f, -153.f)) *
						  glm::rotate(glm::mat4(1.f), glm::radians(-80.f), glm::vec3(1.f, 0.f, 0.f)) *
						  glm::rotate(glm::mat4(1.f), glm::radians(13.f), glm::vec3(0.f, 1.f, 0.f));
	pipe_12.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(7.25f, 7.f, -140.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f));
	pipe_13.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-2.5f, 7.f, -140.5f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(-13.f), glm::vec3(0.f, 1.f, 0.f));
	pipe_14.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-0.2f, 7.f, -150.1f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(-13.f), glm::vec3(0.f, 1.f, 0.f));
	pipe_15.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(2.4f, 7.f, -160.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f));
	////////////////////////////////////////////////////////////////////////////////////////////////////
	corridor_straight_8.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 10.f, -160.f));
	wall_13.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 7.5f, -170.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(1.f, 0.f, 0.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f));
	iris_10.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-3.f, 7.5f, -170.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(1.f, 0.f, 0.f));
	cannon_7.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(2.2f, 8.85f, -170.f));
	cannon_8.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.8f, 8.85f, -170.f));
	cannon_mount_5.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, 7.5f, -170.f)) *
								  glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(1.f, 0.f, 0.f)) *
								  glm::rotate(glm::mat4(1.f), glm::radians(20.f), glm::vec3(0.f, 1.f, 0.f));
	corridor_fork_4.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 5.f, -200.f)) *
								   glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(0.f, 1.f, 0.f)) *
								   glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f));

	corridor_straight_9.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 5.f, -200.f));
	corridor_right_6.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 5.f, -220.f)) *
									glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(0.f, 0.f, 1.f));
	pipe_16.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, 8.5f, -230.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f));
	pipe_17.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 8.5f, -230.f));
	beam_5.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 4.f, -236.f)) *
						  glm::rotate(glm::mat4(1.f), glm::radians(45.f), glm::vec3(0.f, 0.f, 1.f));
	corridor_straight_10.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 5.f, -240.f));
	pipe_18.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 8.5f, -240.f));
	pipe_19.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, 8.5f, -250.f)) *
						   glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f));
	wall_14.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-8.f, 5.f, -260.f));
	wall_15.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(2.f, 5.f, -260.f));


	this->m_world_matrix = glm::scale(glm::mat4(1.f), glm::vec3(0.02, 0.02, 0.02));

	//this->m_world_matrix = glm::mat4(1.f);
}

void Renderer::InitCamera()
{
	this->m_camera_position = glm::vec3(0, 0, 0);
	this->m_camera_target_position = glm::vec3(0, 0, -1);
	this->m_camera_up_vector = glm::vec3(0, 1, 0);

	this->m_view_matrix = glm::lookAt(this->m_camera_position, this->m_camera_target_position, m_camera_up_vector);

	this->m_projection_matrix = glm::perspective(glm::radians(45.f), this->m_screen_width / (float)this->m_screen_height, 0.02f, 100.f);

	GeometryNode& cannon = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_GUNNER];

	glm::mat4 cannon_to_world_space = glm::translate(glm::mat4(1.f), this->m_camera_position) * m_world_matrix *
									  glm::rotate(glm::mat4(1.f), glm::radians(-20.f), glm::vec3(1.f, 0.f, 0.f)) *
									  glm::rotate(glm::mat4(1.f), glm::radians(200.f), glm::vec3(0.f, 1.f, 0.f));
	cannon_to_world_space = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -0.05f, -0.15f)) * cannon_to_world_space;
	cannon.model_matrix = glm::inverse(m_world_matrix) * cannon_to_world_space;
	//cannon.m_aabb.center = glm::vec3(cannon.model_matrix * glm::vec4(cannon.m_aabb.center, 1.f));
}

bool Renderer::InitLights()
{
	this->m_light.SetColor(glm::vec3(0.2f));
	this->m_light.SetPosition(glm::vec3(0, 1, 0));
	this->m_light.SetTarget(glm::vec3(0, 0, -0.1));
	this->m_light.SetConeSize(25, 50);
	this->m_light.CastShadow(false);
	return true;
}

bool Renderer::InitShaders()
{
	std::string vertex_shader_path = "Assets/Shaders/geometry pass.vert";
	std::string geometry_shader_path = "Assets/Shaders/geometry pass.geom";
	std::string fragment_shader_path = "Assets/Shaders/geometry pass.frag";

	m_geometry_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_geometry_program.LoadGeometryShaderFromFile(geometry_shader_path.c_str());
	m_geometry_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_geometry_program.CreateProgram();

	vertex_shader_path = "Assets/Shaders/deferred pass.vert";
	fragment_shader_path = "Assets/Shaders/deferred pass.frag";

	m_deferred_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_deferred_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_deferred_program.CreateProgram();

	vertex_shader_path = "Assets/Shaders/post_process.vert";
	fragment_shader_path = "Assets/Shaders/post_process.frag";

	m_post_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_post_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_post_program.CreateProgram();

	vertex_shader_path = "Assets/Shaders/shadow_map_rendering.vert";
	fragment_shader_path = "Assets/Shaders/shadow_map_rendering.frag";

	m_spot_light_shadow_map_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_spot_light_shadow_map_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_spot_light_shadow_map_program.CreateProgram();

	return true;
}

bool Renderer::InitIntermediateBuffers()
{
	glGenTextures(1, &m_fbo_depth_texture);
	glGenTextures(1, &m_fbo_pos_texture);
	glGenTextures(1, &m_fbo_normal_texture);
	glGenTextures(1, &m_fbo_albedo_texture);
	glGenTextures(1, &m_fbo_mask_texture);
	glGenTextures(1, &m_fbo_texture);

	glGenFramebuffers(1, &m_fbo);

	return ResizeBuffers(m_screen_width, m_screen_height);
}

bool Renderer::ResizeBuffers(int width, int height)
{
	m_screen_width = width;
	m_screen_height = height;

	// texture
	glBindTexture(GL_TEXTURE_2D, m_fbo_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_pos_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	
	glBindTexture(GL_TEXTURE_2D, m_fbo_normal_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_albedo_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_mask_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_depth_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, m_screen_width, m_screen_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

	// framebuffer to link to everything together
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbo_pos_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_fbo_normal_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_fbo_albedo_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, m_fbo_mask_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_fbo_depth_texture, 0);

	GLenum status = Tools::CheckFramebufferStatus(m_fbo);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		return false;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return true;
}

bool Renderer::InitCommonItems()
{
	glGenVertexArrays(1, &m_vao_fbo);
	glBindVertexArray(m_vao_fbo);

	GLfloat fbo_vertices[] = {
		-1, -1,
		1, -1,
		-1, 1,
		1, 1, };

	glGenBuffers(1, &m_vbo_fbo_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_fbo_vertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fbo_vertices), fbo_vertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
	return true;
}

bool Renderer::InitGeometricMeshes()
{
	//ALL_PARTS
	std::array<const char*, GEOMETRY_OBJECTS::SIZE_ALL_GEO> geometry_assets = {
		"Assets/assets/Cannon.obj",
		"Assets/assets/Cannon.obj",
		"Assets/assets/Cannon.obj",
		"Assets/assets/Cannon.obj",
		"Assets/assets/Cannon.obj",
		"Assets/assets/Cannon.obj",
		"Assets/assets/Cannon.obj",
		"Assets/assets/Cannon.obj",
		"Assets/assets/Cannon.obj",
		"Assets/assets/CannonMount.obj",
		"Assets/assets/CannonMount.obj",
		"Assets/assets/CannonMount.obj",
		"Assets/assets/CannonMount.obj",
		"Assets/assets/CannonMount.obj",
		"Assets/assets/Iris.obj",
		"Assets/assets/Iris.obj",
		"Assets/assets/Iris.obj",
		"Assets/assets/Iris.obj",
		"Assets/assets/Iris.obj",
		"Assets/assets/Iris.obj",
		"Assets/assets/Iris.obj",
		"Assets/assets/Iris.obj",
		"Assets/assets/Iris.obj",
		"Assets/assets/Iris.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Pipe.obj",
		"Assets/assets/Sphere.obj",
		"Assets/assets/Sphere.obj",
		"Assets/assets/Sphere.obj",

	};

	std::array<const char*, COLLIDABLES_OBJECTS::SIZE_ALL_COLL> collidable_assets = {
		"Assets/assets/Beam.obj",
		"Assets/assets/Beam.obj",
		"Assets/assets/Beam.obj",
		"Assets/assets/Beam.obj",
		"Assets/assets/Beam.obj",
		"Assets/assets/Corridor_Fork.obj",
		"Assets/assets/Corridor_Fork.obj",
		"Assets/assets/Corridor_Fork.obj",
		"Assets/assets/Corridor_Fork.obj",
		"Assets/assets/Corridor_Right.obj",
		"Assets/assets/Corridor_Right.obj",
		"Assets/assets/Corridor_Right.obj",
		"Assets/assets/Corridor_Right.obj",
		"Assets/assets/Corridor_Right.obj",
		"Assets/assets/Corridor_Right.obj",
		"Assets/assets/Corridor_Straight.obj",
		"Assets/assets/Corridor_Straight.obj",
		"Assets/assets/Corridor_Straight.obj",
		"Assets/assets/Corridor_Straight.obj",
		"Assets/assets/Corridor_Straight.obj",
		"Assets/assets/Corridor_Straight.obj",
		"Assets/assets/Corridor_Straight.obj",
		"Assets/assets/Corridor_Straight.obj",
		"Assets/assets/Corridor_Straight.obj",
		"Assets/assets/Corridor_Straight.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
		"Assets/assets/Wall.obj",
	};


	bool initialized = true;
	OBJLoader loader;

	for (int32_t i = 0; i < GEOMETRY_OBJECTS::SIZE_ALL_GEO; ++i) {
		auto& asset = geometry_assets[i];
		GeometricMesh* mesh = loader.load(asset);

		if (mesh != nullptr) {
			GeometryNode* node = new GeometryNode();
			node->Init(mesh);
			this->m_nodes.push_back(node);
			delete mesh;
		}
		else {
			initialized = false;
		}
	}

	for (int32_t i = 0; i < COLLIDABLES_OBJECTS::SIZE_ALL_COLL; ++i) {
		auto& asset = collidable_assets[i];
		GeometricMesh* mesh = loader.load(asset);

		if (mesh != nullptr) {
			CollidableNode* node = new CollidableNode();
			node->Init(mesh);
			this->m_collidables_nodes.push_back(node);
			delete mesh;
		}
		else {
			initialized = false;
		}
	}

	/*GeometricMesh* mesh = loader.load(assets[OBJECTS::CORRIDOR_STRAIGHT_1]);

	if (mesh != nullptr)
	{
		CollidableNode* node = new CollidableNode();
		node->Init(mesh);
		this->m_collidables_nodes.push_back(node);
		delete mesh;
	}*/

	return initialized;
}

void Renderer::Update(float dt, bool shooting_pose, bool shooting_enable, bool shooting_gunner) {
	m_continous_time += dt;
	this->UpdateGeometry(dt);
	this->UpdateCamera(dt, shooting_pose, shooting_enable, shooting_gunner);
}

void Renderer::UpdateGeometry(float dt) {

	/*CollidableNode& beam = *this->m_collidables_nodes[0];
	beam.app_model_matrix = beam.model_matrix;*/

	//FIRST_PART_DECLARATIONS
	CollidableNode& corridor_straight_1 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_1];
	GeometryNode& pipe_1 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_1];
	GeometryNode& pipe_2 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_2];
	//CollidableNode& wall_1 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_1];
	//CollidableNode& wall_2 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_2];
	CollidableNode& corridor_straight_2 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_2];
	GeometryNode& pipe_3 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_3];
	GeometryNode& pipe_4 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_4];
	GeometryNode& pipe_5 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_5];
	CollidableNode& corridor_fork_1 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_FORK_1];
	CollidableNode& wall_3 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_3];
	//GeometryNode& cannon_mount_1 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_1];
	//GeometryNode& cannon_mount_2 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_2];
	//GeometryNode& cannon_1 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_1];
	//GeometryNode& cannon_2 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_2];
	CollidableNode& beam_1 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::BEAM_1];

	CollidableNode& corridor_right_1 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_1];
	CollidableNode& corridor_straight_3 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_3];
	CollidableNode& wall_4 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_4];
	GeometryNode& cannon_mount_3 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_3];
	//GeometryNode& cannon_3 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_3];
	//GeometryNode& cannon_4 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_4];
	GeometryNode& iris_1 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_1];
	CollidableNode& wall_5 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_5];
	GeometryNode& iris_2 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_2];
	GeometryNode& iris_3 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_3];
	CollidableNode& corridor_straight_5 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_5];
	GeometryNode& pipe_6 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_6];
	GeometryNode& pipe_7 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_7];
	CollidableNode& beam_2 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::BEAM_2];
	CollidableNode& corridor_straight_6 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_6];
	GeometryNode& iris_7 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_7];
	GeometryNode& pipe_8 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_8];
	GeometryNode& pipe_9 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_9];
	GeometryNode& pipe_10 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_10];
	GeometryNode& pipe_11 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_11];

	CollidableNode& corridor_right_2 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_2];
	CollidableNode& corridor_straight_4 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_4];
	CollidableNode& wall_6 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_6];
	GeometryNode& cannon_mount_4 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_4];
	//GeometryNode& cannon_5 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_5];
	//GeometryNode& cannon_6 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_6];
	GeometryNode& iris_4 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_4];
	CollidableNode& wall_7 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_7];
	GeometryNode& iris_5 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_5];
	GeometryNode& iris_6 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_6];
	CollidableNode& corridor_fork_2 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_FORK_2];
	CollidableNode& beam_3 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::BEAM_3];
	CollidableNode& corridor_right_3 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_3];

	//GeometryNode& sphere_1 = *this->m_nodes[GEOMETRY_OBJECTS::SPHERE_1];
	//GeometryNode& sphere_2 = *this->m_nodes[GEOMETRY_OBJECTS::SPHERE_2];
	//GeometryNode& sphere_3 = *this->m_nodes[GEOMETRY_OBJECTS::SPHERE_3];

	//FIRST_PART_TRANSFORMATIONS
	corridor_straight_1.app_model_matrix = corridor_straight_1.model_matrix;
	pipe_1.app_model_matrix = pipe_1.model_matrix;
	pipe_2.app_model_matrix = pipe_2.model_matrix;
	//wall_1.app_model_matrix = wall_1.model_matrix;
	//wall_2.app_model_matrix = wall_2.model_matrix;
	corridor_straight_2.app_model_matrix = corridor_straight_2.model_matrix;
	pipe_3.app_model_matrix = pipe_3.model_matrix;
	pipe_4.app_model_matrix = pipe_4.model_matrix;
	pipe_5.app_model_matrix = pipe_5.model_matrix;
	corridor_fork_1.app_model_matrix = corridor_fork_1.model_matrix;
	wall_3.app_model_matrix = wall_3.model_matrix;
	//cannon_mount_1.app_model_matrix = cannon_mount_1.model_matrix;
	//cannon_mount_2.app_model_matrix = cannon_mount_2.model_matrix;
	//cannon_1.app_model_matrix = cannon_1.model_matrix;
	//cannon_2.app_model_matrix = cannon_2.model_matrix;
	beam_1.app_model_matrix = beam_1.model_matrix;

	corridor_right_1.app_model_matrix = corridor_right_1.model_matrix;
	corridor_straight_3.app_model_matrix = corridor_straight_3.model_matrix;
	wall_4.app_model_matrix = wall_4.model_matrix;
	cannon_mount_3.app_model_matrix = cannon_mount_3.model_matrix;
	//cannon_3.app_model_matrix = cannon_3.model_matrix;
	//cannon_4.app_model_matrix = cannon_4.model_matrix;
	iris_1.app_model_matrix = iris_1.model_matrix;
	wall_5.app_model_matrix = wall_5.model_matrix;
	iris_2.app_model_matrix = iris_2.model_matrix;
	iris_3.app_model_matrix = iris_3.model_matrix;
	corridor_straight_5.app_model_matrix = corridor_straight_5.model_matrix;
	pipe_6.app_model_matrix = pipe_6.model_matrix;
	pipe_7.app_model_matrix = pipe_7.model_matrix;
	beam_2.app_model_matrix = beam_2.model_matrix;
	corridor_straight_6.app_model_matrix = corridor_straight_6.model_matrix;
	iris_7.app_model_matrix = iris_7.model_matrix;
	pipe_8.app_model_matrix = pipe_8.model_matrix;
	pipe_9.app_model_matrix = pipe_9.model_matrix;
	pipe_10.app_model_matrix = pipe_10.model_matrix;
	pipe_11.app_model_matrix = pipe_11.model_matrix;

	corridor_right_2.app_model_matrix = corridor_right_2.model_matrix;
	corridor_straight_4.app_model_matrix = corridor_straight_4.model_matrix;
	wall_6.app_model_matrix = wall_6.model_matrix;
	cannon_mount_4.app_model_matrix = cannon_mount_4.model_matrix;
	//cannon_5.app_model_matrix = cannon_5.model_matrix;
	//cannon_6.app_model_matrix = cannon_6.model_matrix;
	iris_4.app_model_matrix = iris_4.model_matrix;
	wall_7.app_model_matrix = wall_7.model_matrix;
	iris_5.app_model_matrix = iris_5.model_matrix;
	iris_6.app_model_matrix = iris_6.model_matrix;
	corridor_fork_2.app_model_matrix = corridor_fork_2.model_matrix;
	beam_3.app_model_matrix = beam_3.model_matrix;
	corridor_right_3.app_model_matrix = corridor_right_3.model_matrix;


	//SECOND_PART_DECLARATIONS
	CollidableNode& corridor_fork_3 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_FORK_3];
	CollidableNode& corridor_right_5 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_5];
	CollidableNode& wall_11 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_11];
	CollidableNode& wall_12 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_12];
	CollidableNode& beam_4 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::BEAM_4];

	CollidableNode& corridor_straight_7 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_7];
	CollidableNode& wall_8 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_8];
	CollidableNode& wall_9 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_9];
	CollidableNode& corridor_right_4 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_4];
	CollidableNode& wall_10 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_10];
	GeometryNode& iris_8 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_8];
	GeometryNode& iris_9 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_9];
	GeometryNode& pipe_12 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_12];
	GeometryNode& pipe_13 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_13];
	GeometryNode& pipe_14 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_14];
	GeometryNode& pipe_15 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_15];
	CollidableNode& corridor_straight_8 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_8];
	CollidableNode& wall_13 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_13];
	GeometryNode& iris_10 = *this->m_nodes[GEOMETRY_OBJECTS::IRIS_10];
	GeometryNode& cannon_7 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_7];
	GeometryNode& cannon_8 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_8];
	GeometryNode& cannon_mount_5 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_5];
	CollidableNode& corridor_fork_4 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_FORK_4];

	CollidableNode& corridor_straight_9 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_9];
	CollidableNode& corridor_right_6 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_RIGHT_6];
	GeometryNode& pipe_16 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_16];
	GeometryNode& pipe_17 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_17];
	CollidableNode& beam_5 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::BEAM_5];
	CollidableNode& corridor_straight_10 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::CORRIDOR_STRAIGHT_10];
	GeometryNode& pipe_18 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_18];
	GeometryNode& pipe_19 = *this->m_nodes[GEOMETRY_OBJECTS::PIPE_19];
	CollidableNode& wall_14 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_14];
	CollidableNode& wall_15 = *this->m_collidables_nodes[COLLIDABLES_OBJECTS::WALL_15];

	

	//SECOND_PART_TRANSFORMATIONS
	corridor_fork_3.app_model_matrix = corridor_fork_3.model_matrix;
	corridor_right_5.app_model_matrix = corridor_right_5.model_matrix;
	//wall_11.app_model_matrix = wall_11.model_matrix;
	//wall_12.app_model_matrix = wall_12.model_matrix;
	beam_4.app_model_matrix = beam_4.model_matrix;

	corridor_straight_7.app_model_matrix = corridor_straight_7.model_matrix;
	//wall_8.app_model_matrix = wall_8.model_matrix;
	//wall_9.app_model_matrix = wall_9.model_matrix;
	corridor_right_4.app_model_matrix = corridor_right_4.model_matrix;
	wall_10.app_model_matrix = wall_10.model_matrix;
	iris_8.app_model_matrix = iris_8.model_matrix;
	iris_9.app_model_matrix = iris_9.model_matrix;
	pipe_12.app_model_matrix = pipe_12.model_matrix;
	pipe_13.app_model_matrix = pipe_13.model_matrix;
	pipe_14.app_model_matrix = pipe_14.model_matrix;
	pipe_15.app_model_matrix = pipe_15.model_matrix;
	corridor_straight_8.app_model_matrix = corridor_straight_8.model_matrix;
	wall_13.app_model_matrix = wall_13.model_matrix;
	iris_10.app_model_matrix = iris_10.model_matrix;
	//cannon_7.app_model_matrix = cannon_7.model_matrix;
	//cannon_8.app_model_matrix = cannon_8.model_matrix;
	//cannon_mount_5.app_model_matrix = cannon_mount_5.model_matrix;
	corridor_fork_4.app_model_matrix = corridor_fork_4.model_matrix;

	corridor_straight_9.app_model_matrix = corridor_straight_9.model_matrix;
	corridor_right_6.app_model_matrix = corridor_right_6.model_matrix;
	pipe_16.app_model_matrix = pipe_16.model_matrix;
	pipe_17.app_model_matrix = pipe_17.model_matrix;
	beam_5.app_model_matrix = beam_5.model_matrix;
	corridor_straight_10.app_model_matrix = corridor_straight_10.model_matrix;
	pipe_18.app_model_matrix = pipe_18.model_matrix;
	pipe_19.app_model_matrix = pipe_19.model_matrix;
	wall_14.app_model_matrix = wall_14.model_matrix;
	wall_15.app_model_matrix = wall_15.model_matrix;

}

void Renderer::UpdateCamera(float dt, bool shooting_pose, bool shooting_enable, bool shooting_gunner)
{
	glm::vec3 direction = glm::normalize(m_camera_target_position - m_camera_position);

	glm::vec3 right = glm::normalize(glm::cross(direction, m_camera_up_vector));

	glm::vec3 camera_up_vector = glm::vec3(0, 1, 0);
	glm::vec3 camera_down_vector = glm::vec3(0, -1, 0);
	glm::vec3 camera_left_vector = glm::vec3(-1, 0, 0);
	glm::vec3 camera_right_vector = glm::vec3(1, 0, 0);

	glm::vec3 new_camera_position = m_camera_position;
	glm::vec3 last_camera_position = new_camera_position;
	new_camera_position = new_camera_position + (m_camera_movement.x * 0.1f * dt) * direction;
	new_camera_position = new_camera_position + (m_camera_movement.y * 0.1f * dt) * right;

	m_camera_position = m_camera_position + (m_camera_movement.x * 0.1f * dt) * direction;//
	m_camera_position = m_camera_position + (m_camera_movement.y * 0.1f * dt) * right;//
	m_camera_position = m_camera_position + (m_camera_movement.z * 0.1f * dt) * m_camera_up_vector;

	bool check_new_pos = false;
	for (auto& node : this->m_collidables_nodes) {
		float_t isectT_dir = 0.f;
		float_t isectT_right = 0.f;
		float_t isectT_up = 0.f;
		float_t isectT_left = 0.f;
		float_t isectT_down = 0.f;
		int32_t primID = -1;
		node->intersectRay(new_camera_position, direction, m_world_matrix, isectT_dir, primID);
		node->intersectRay(new_camera_position, camera_up_vector, m_world_matrix, isectT_up, primID);
		node->intersectRay(new_camera_position, camera_down_vector, m_world_matrix, isectT_down, primID);
		node->intersectRay(new_camera_position, camera_left_vector, m_world_matrix, isectT_left, primID);
		node->intersectRay(new_camera_position, camera_right_vector, m_world_matrix, isectT_right, primID);
		//std::cout << "DIR " << isectT_dir << " LEFT " << isectT_left << " RIGHT " << isectT_right << " UP " << isectT_up << std::endl;
		if (isectT_dir > 0.25f && isectT_right > 0.25f && isectT_up > 0.25f && isectT_left > 0.25f && isectT_down > 0.25f) {
			if (isectT_right < 30.f && isectT_up < 30.f && isectT_left < 30.f && isectT_down < 30.f) {
				check_new_pos = true;
			}
		}
	}

	if (check_new_pos) {
		m_camera_position = new_camera_position;
	}

	m_camera_target_position = m_camera_target_position + (m_camera_movement.x * 1.f * dt) * direction;
	m_camera_target_position = m_camera_target_position + (m_camera_movement.y * 1.f * dt) * right;

	/*m_camera_position = m_camera_position + (m_camera_movement.x * 1.f * dt) * direction;
	m_camera_target_position = m_camera_target_position + (m_camera_movement.x * 1.f * dt) * direction;

	m_camera_position = m_camera_position + (m_camera_movement.y * 1.f * dt) * right;
	m_camera_target_position = m_camera_target_position + (m_camera_movement.y * 1.f * dt) * right;*/

	float speed = glm::pi<float>() * 0.002;
	glm::mat4 rotation = glm::rotate(glm::mat4(1.f), m_camera_look_angle_destination.y * speed, right);
	rotation *= glm::rotate(glm::mat4(1.f), m_camera_look_angle_destination.x * speed, m_camera_up_vector);

	float destination_x = m_camera_look_angle_destination.x;
	float destination_y = m_camera_look_angle_destination.y;
	sum_x += destination_x;
	sum_y += destination_y;
	float rot_x = (360.f * sum_y) / 1000.f;
	float rot_y = (360.f * sum_x) / 1000.f;
	//std::cout << " " << rot_y << " " << sum_x << " " << m_camera_look_angle_destination.x << std::endl;

	std::cout << "||||||" << " y " << m_camera_look_angle_destination.y << " x " << m_camera_look_angle_destination.x << std::endl;

	m_camera_look_angle_destination = glm::vec2(0.f);

	direction = rotation * glm::vec4(direction, 0.f);

	glm::vec3 camera_direction_1 = glm::normalize(m_camera_position - glm::vec3(m_camera_position.x, m_camera_position.y, m_camera_position.z - 1));
	float camera_theta = glm::acos(glm::dot(camera_direction_1, direction));
	float camera_degrees = (camera_theta * 180.0f) / glm::pi<float>();
	if (camera_degrees < 40.f || camera_degrees > -40.f) {
		m_camera_target_position = m_camera_position + direction * glm::distance(m_camera_position, m_camera_target_position);
		m_view_matrix = glm::lookAt(m_camera_position, m_camera_target_position, m_camera_up_vector);
	}


	GeometryNode& cannon = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_GUNNER];
	GeometryNode& sphere_1 = *this->m_nodes[GEOMETRY_OBJECTS::SPHERE_1];
	GeometryNode& sphere_2 = *this->m_nodes[GEOMETRY_OBJECTS::SPHERE_2];
	GeometryNode& sphere_3 = *this->m_nodes[GEOMETRY_OBJECTS::SPHERE_3];

	if (shooting_pose && !shooting_enable) {
		glm::mat4 cannon_to_world_space = glm::translate(glm::mat4(1.f), this->m_camera_position) * 
										  glm::rotate(glm::mat4(1.f), glm::radians(rot_x), glm::vec3(/*1.f, 0.f, 0.f*/right)) *
										  glm::rotate(glm::mat4(1.f), glm::radians(rot_y), glm::vec3(/*0.f, 1.f, 0.f*/m_camera_up_vector)) *
										  m_world_matrix *
										  glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(0.f, 1.f, 0.f));
		cannon_to_world_space = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -0.05f, -0.15f)) * 
								m_view_matrix *
								cannon_to_world_space;
		cannon.app_model_matrix = glm::inverse(m_world_matrix) * 
								  glm::inverse(m_view_matrix) * 
			                      cannon_to_world_space;
	} else if (shooting_pose && shooting_enable) {
		glm::mat4 cannon_to_world_space = glm::translate(glm::mat4(1.f), this->m_camera_position) * 
										  glm::rotate(glm::mat4(1.f), glm::radians(rot_x), glm::vec3(/*1.f, 0.f, 0.f*/right)) *
										  glm::rotate(glm::mat4(1.f), glm::radians(rot_y), glm::vec3(/*0.f, 1.f, 0.f*/m_camera_up_vector)) *
										  m_world_matrix *
										  glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(0.f, 1.f, 0.f));
		if (shooting_enable && shooting_gunner) {
			cannon_to_world_space = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -0.045f, -0.15f)) *
									m_view_matrix *
									cannon_to_world_space;
			cannon.app_model_matrix = glm::inverse(m_world_matrix) * 
									  glm::inverse(m_view_matrix) *
									  cannon_to_world_space;
		}
		else {
			cannon_to_world_space = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -0.05f, -0.15f)) * 
									m_view_matrix *
									cannon_to_world_space;
			cannon.app_model_matrix = glm::inverse(m_world_matrix) * 
									  glm::inverse(m_view_matrix) *
									  cannon_to_world_space;
		}
	} else if (!shooting_pose) {
		glm::mat4 cannon_to_world_space = glm::translate(glm::mat4(1.f), this->m_camera_position) *
										  glm::rotate(glm::mat4(1.f), glm::radians(rot_x), glm::vec3(/*1.f, 0.f, 0.f*/right)) *
										  glm::rotate(glm::mat4(1.f), glm::radians(rot_y), glm::vec3(/*0.f, 1.f, 0.f*/m_camera_up_vector)) *
										  m_world_matrix *
										  glm::rotate(glm::mat4(1.f), glm::radians(-20.f), glm::vec3(1.f, 0.f, 0.f)) *
										  glm::rotate(glm::mat4(1.f), glm::radians(200.f), glm::vec3(0.f, 1.f, 0.f));
		cannon_to_world_space = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -0.05f, -0.15f)) *
							    m_view_matrix *
								cannon_to_world_space;
		cannon.app_model_matrix = glm::inverse(m_world_matrix) *
								  glm::inverse(m_view_matrix) *
								  cannon_to_world_space;

	}

	for (auto i = 0; i < 3; ++i) {
		if (spheres_gunner_array[i]) {
			sphere_position[i] += dt * 5.f;
			float coefficient = 1.f;
			glm::vec3 gunner_direction_y = glm::normalize(sphere_initial_position[i] - glm::vec3(sphere_initial_position[i].x, sphere_initial_target_position[i].y, sphere_initial_target_position[i].z));
			glm::vec3 gunner_direction_x = glm::normalize(sphere_initial_position[i] - glm::vec3(sphere_initial_target_position[i].x, sphere_initial_position[i].y, sphere_initial_target_position[i].z));
			glm::vec3 gunner_flat_direction = glm::normalize(sphere_initial_position[i] - glm::vec3(sphere_initial_position[i].x, sphere_initial_position[i].y, sphere_initial_position[i].z - 1.f));
			if (spheres_update_theta_array[i]) {
				spheres_update_theta_array[i] = false;
				spheres_theta_y_array[i] = glm::acos(glm::dot(gunner_direction_y, gunner_flat_direction));
				spheres_theta_x_array[i] = glm::acos(glm::dot(gunner_direction_x, gunner_flat_direction));
				if (m_camera_target_position.y < m_camera_position.y) {
					spheres_theta_y_array[i] = -spheres_theta_y_array[i];
				}
				if (m_camera_target_position.x < m_camera_position.x) {
					spheres_theta_x_array[i] = -spheres_theta_x_array[i];
				}
			}
			float distance_y = glm::tan(spheres_theta_y_array[i]) * sphere_position[i];
			float distance_x = glm::tan(spheres_theta_x_array[i]) * sphere_position[i];
			GeometryNode& sphere = *this->m_nodes[SIZE_ALL_GEO - i - 1];
			sphere.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(sphere_initial_position[i].x + distance_x, sphere_initial_position[i].y + distance_y, sphere_initial_position[i].z - sphere_position[i])) *
									  glm::scale(glm::mat4(1.f), glm::vec3(0.1, 0.1, 0.1));
			if (sphere_position[i] > 20.f) {
				sphere_position[i] = 0.f;
				spheres_gunner_array[i] = false;
				sphere.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f));
			}

		}
	}
	for (auto i = 0; i < 3; ++i) {
		if ((shooting_pose && shooting_enable) && !spheres_gunner_array[i]) {
			float gunner_pose = 0.f;
			std::cout << "**" << std::endl;
			if (shooting_gunner) {
				gunner_pose = -0.045f;
			}
			else {
				gunner_pose = -0.05f;
			}
			spheres_update_theta_array[i] = true;
			spheres_theta_y_array[i] = 0.f;
			spheres_theta_x_array[i] = 0.f;
			sphere_initial_position[i].x = m_camera_position.x * 50.f;
			sphere_initial_position[i].y = m_camera_position.y * 50.f + gunner_pose;
			sphere_initial_position[i].z = m_camera_position.z * 50.f + -0.15f;
			sphere_initial_target_position[i] = m_camera_target_position * 50.f;
			spheres_gunner_array[i] = true;
			break;
		}
		/*float camera_position_x = m_camera_position.x * 50.f;
		float camera_position_y = m_camera_position.y * 50.f;
		float camera_position_z = m_camera_position.z * 50.f;
		std::cout << "|||1 " << camera_position_x << " " << camera_position_y << " " << camera_position_z << " " << std::endl;
		std::cout << "|||2 " << sphere_initial_position[i].x << " " << sphere_initial_position[i].y << " " << sphere_initial_position[i].z << " " << std::endl;*/
	}


	UpdateCircuitMovingGeometry(dt);


	//std::cout << m_camera_position.x << " " << m_camera_position.y << " " << m_camera_position.z << " " << std::endl;
	//std::cout << m_camera_target_position.x << " " << m_camera_target_position.y << " " << m_camera_target_position.z << " " << std::endl;
	m_light.SetPosition(m_camera_position);
	m_light.SetTarget(m_camera_target_position);
	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Renderer::UpdateCircuitMovingGeometry(float dt) {
	float camera_position_x = m_camera_position.x * 50.f;
	float camera_position_y = m_camera_position.y * 50.f;
	float camera_position_z = m_camera_position.z * 50.f;

	GeometryNode& wall_1 = *this->m_nodes[COLLIDABLES_OBJECTS::WALL_1];
	GeometryNode& wall_2 = *this->m_nodes[COLLIDABLES_OBJECTS::WALL_2];
	GeometryNode& cannon_1 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_1];
	GeometryNode& cannon_mount_1 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_1];
	GeometryNode& cannon_2 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_2];
	GeometryNode& cannon_mount_2 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_2];
	GeometryNode& cannon_3 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_3];
	GeometryNode& cannon_4 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_4];
	GeometryNode& cannon_mount_3 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_3];
	GeometryNode& cannon_5 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_5];
	GeometryNode& cannon_6 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_6];
	GeometryNode& cannon_mount_4 = *this->m_nodes[GEOMETRY_OBJECTS::CANNON_MOUNT_4];

	
	if (camera_position_z <= -3.f && camera_position_z >= -21.f) {
		if (last_position_first_walls - 2.f * dt < -3.f) {
			last_position_first_walls = -3.f;
		} else {
			last_position_first_walls -= 2.f * dt;
		}
		wall_1.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-3.0f + last_position_first_walls, 0.f, -20.f));
		wall_2.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.0f -last_position_first_walls, 0.f, -20.f));
	} else {
		last_position_first_walls = 0.f;
		wall_1.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-3.f, 0.f, -20.f));
		wall_2.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, 0.f, -20.f));
	}

	

	glm::vec3 direction_width_camera_to_cannon_1 = glm::normalize(glm::vec3(-2.2f, -1.65f, -45.f) - glm::vec3(camera_position_x, -1.65f, camera_position_z));
	glm::vec3 direction_height_camera_to_cannon_1 = glm::normalize(glm::vec3(-2.2f, -1.65f, -45.f) - glm::vec3(-2.2f, camera_position_y, camera_position_z));
	glm::vec3 direction_cannon_1 = glm::normalize(glm::vec3(-2.2f, -1.65f, -45.f) - glm::vec3(-2.2f, -1.65f, 0.f));
	float theta_width_cannon_1 = glm::acos(glm::dot(direction_width_camera_to_cannon_1, direction_cannon_1));
	float theta_height_cannon_1 = glm::acos(glm::dot(direction_height_camera_to_cannon_1, direction_cannon_1));
	float degrees_width_cannon_1 = (theta_width_cannon_1 * 180.0f) / glm::pi<float>();
	float degrees_height_cannon_1 = (theta_height_cannon_1 * 180.0f) / glm::pi<float>();
	if (camera_position_x < -2.2f) {
		theta_width_cannon_1 = -theta_width_cannon_1;
		degrees_width_cannon_1 = -degrees_width_cannon_1;
	}
	if (camera_position_y > -1.65f) {
		theta_height_cannon_1 = -theta_height_cannon_1;
		degrees_height_cannon_1 = -degrees_height_cannon_1;
	}
	//std::cout << " Width - Theta: " << theta_width_cannon_1 << " Degrees: " << degrees_width_cannon_1 << std::endl;
	//std::cout << " Height - Theta: " << theta_height_cannon_1 << " Degrees: " << degrees_height_cannon_1 << std::endl;
	//std::cout << camera_position_x << " " << camera_position_y << " " << camera_position_z << " " << std::endl;

	cannon_1.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-2.2f, -1.65f, -45.f)) *
								glm::rotate(glm::mat4(1.f), theta_height_cannon_1, glm::vec3(1.f, 0.f, 0.f)) *
								glm::rotate(glm::mat4(1.f), theta_width_cannon_1, glm::vec3(0.f, 1.f, 0.f));
	cannon_mount_1.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-3.f, -3.f, -45.f)) *
									  glm::rotate(glm::mat4(1.f), theta_width_cannon_1, glm::vec3(0.f, 1.f, 0.f)) *
									  glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(1.f, 0.f, 0.f));

	glm::vec3 direction_width_camera_to_cannon_2 = glm::normalize(glm::vec3(2.2f, -1.65f, -45.f) - glm::vec3(camera_position_x, -1.65f, camera_position_z));
	glm::vec3 direction_height_camera_to_cannon_2 = glm::normalize(glm::vec3(2.2f, -1.65f, -45.f) - glm::vec3(2.2f, camera_position_y, camera_position_z));
	glm::vec3 direction_cannon_2 = glm::normalize(glm::vec3(2.2f, -1.65f, -45.f) - glm::vec3(2.2f, -1.65f, 0.f));
	float theta_width_cannon_2 = glm::acos(glm::dot(direction_width_camera_to_cannon_2, direction_cannon_2));
	float theta_height_cannon_2 = glm::acos(glm::dot(direction_height_camera_to_cannon_2, direction_cannon_2));
	if (camera_position_x < 2.2f) {
		theta_width_cannon_2 = -theta_width_cannon_2;
	}
	if (camera_position_y > -1.65f) {
		theta_height_cannon_2 = -theta_height_cannon_2;
	}
	cannon_2.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(2.2f, -1.65f, -45.f)) *
								glm::rotate(glm::mat4(1.f), theta_height_cannon_2, glm::vec3(1.f, 0.f, 0.f)) *
								glm::rotate(glm::mat4(1.f), theta_width_cannon_2, glm::vec3(0.f, 1.f, 0.f));
	cannon_mount_2.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, -3.f, -45.f)) *
									  glm::rotate(glm::mat4(1.f), theta_width_cannon_2, glm::vec3(0.f, 1.f, 0.f)) *
									  glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(1.f, 0.f, 0.f));

	glm::vec3 direction_width_camera_to_cannon_3 = glm::normalize(glm::vec3(3.f, -1.2f, -88.5f) - glm::vec3(camera_position_x, -1.2f, camera_position_z));
	glm::vec3 direction_height_camera_to_cannon_3 = glm::normalize(glm::vec3(3.f, -1.2f, -88.5f) - glm::vec3(3.f, camera_position_y, camera_position_z));
	glm::vec3 direction_cannon_3 = glm::normalize(glm::vec3(3.f, -1.2f, -88.5f) - glm::vec3(3.f, -1.2f, 0.f));
	float theta_width_cannon_3 = glm::acos(glm::dot(direction_width_camera_to_cannon_3, direction_cannon_3));
	if (camera_position_x < 3.f) {
		theta_width_cannon_3 = -theta_width_cannon_3;
	}
	cannon_3.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, -1.2f, -88.5f)) *
								glm::rotate(glm::mat4(1.f), theta_width_cannon_3, glm::vec3(0.f, 1.f, 0.f)) *
								glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));

	glm::vec3 direction_width_camera_to_cannon_4 = glm::normalize(glm::vec3(3.f, -2.8f, -88.5f) - glm::vec3(camera_position_x, -2.8f, camera_position_z));
	glm::vec3 direction_height_camera_to_cannon_4 = glm::normalize(glm::vec3(3.f, -2.8f, -88.5f) - glm::vec3(3.f, camera_position_y, camera_position_z));
	glm::vec3 direction_cannon_4 = glm::normalize(glm::vec3(3.f, -2.8f, -88.5f) - glm::vec3(3.f, -2.8f, 0.f));
	float theta_width_cannon_4 = glm::acos(glm::dot(direction_width_camera_to_cannon_4, direction_cannon_4));
	if (camera_position_x < 3.f) {
		theta_width_cannon_4 = -theta_width_cannon_4;
	}
	cannon_4.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, -2.8f, -88.5f)) *
								glm::rotate(glm::mat4(1.f), theta_width_cannon_4, glm::vec3(0.f, 1.f, 0.f)) *
								glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));

	glm::vec3 direction_width_camera_to_cannon_5 = glm::normalize(glm::vec3(-7.f, 8.8f, -88.5f) - glm::vec3(camera_position_x, 8.8f, camera_position_z));
	glm::vec3 direction_height_camera_to_cannon_5 = glm::normalize(glm::vec3(-7.f, 8.8f, -88.5f) - glm::vec3(-7.f, camera_position_y, camera_position_z));
	glm::vec3 direction_cannon_5 = glm::normalize(glm::vec3(-7.f, 8.8f, -88.5f) - glm::vec3(-7.f, 8.8f, 0.f));
	float theta_width_cannon_5 = glm::acos(glm::dot(direction_width_camera_to_cannon_5, direction_cannon_5));
	if (camera_position_x < -7.f) {
		theta_width_cannon_5 = -theta_width_cannon_5;
	}
	cannon_5.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-7.f, 8.8f, -88.5f)) *
								glm::rotate(glm::mat4(1.f), theta_width_cannon_5, glm::vec3(0.f, 1.f, 0.f)) *
								glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));

	glm::vec3 direction_width_camera_to_cannon_6 = glm::normalize(glm::vec3(-7.f, 7.2f, -88.5f) - glm::vec3(camera_position_x, 7.2f, camera_position_z));
	glm::vec3 direction_height_camera_to_cannon_6 = glm::normalize(glm::vec3(-7.f, 7.2f, -88.5f) - glm::vec3(-7.f, camera_position_y, camera_position_z));
	glm::vec3 direction_cannon_6 = glm::normalize(glm::vec3(-7.f, 7.2f, -88.5f) - glm::vec3(-7.f, 7.2f, 0.f));
	float theta_width_cannon_6 = glm::acos(glm::dot(direction_width_camera_to_cannon_6, direction_cannon_6));
	if (camera_position_x < -7.f) {
		theta_width_cannon_6 = -theta_width_cannon_6;
	}

	cannon_6.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-7.f, 7.2f, -88.5f)) *
								glm::rotate(glm::mat4(1.f), theta_width_cannon_6, glm::vec3(0.f, 1.f, 0.f)) *
								glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));

	/*GeometryNode& cannon_7 = *this->m_nodes[OBJECTS::CANNON_7];
	GeometryNode& cannon_8 = *this->m_nodes[OBJECTS::CANNON_8];
	GeometryNode& cannon_mount_5 = *this->m_nodes[OBJECTS::CANNON_MOUNT_5];
	GeometryNode& wall_8 = *this->m_nodes[OBJECTS::WALL_8];
	GeometryNode& wall_9 = *this->m_nodes[OBJECTS::WALL_9];
	GeometryNode& wall_11 = *this->m_nodes[OBJECTS::WALL_11];
	GeometryNode& wall_12 = *this->m_nodes[OBJECTS::WALL_12];

	glm::vec3 direction_width_camera_to_cannon_mount_5 = glm::normalize(glm::vec3(3.f, 7.5f, -170.f) - glm::vec3(camera_position_x, 7.5f, camera_position_z));
	glm::vec3 direction_cannon_mount_5 = glm::normalize(glm::vec3(3.f, 7.5f, -170.f) - glm::vec3(3.f, 7.5f, 0.f));
	float theta_width_cannon_mount_5 = glm::acos(glm::dot(direction_width_camera_to_cannon_mount_5, direction_cannon_mount_5));
	if (camera_position_x < 3.f) {
		theta_width_cannon_mount_5 = -theta_width_cannon_mount_5;
	}
	cannon_mount_5.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.f, 7.5f, -170.f)) *
									  glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(1.f, 0.f, 0.f)) *
									  glm::rotate(glm::mat4(1.f), -theta_width_cannon_mount_5, glm::vec3(0.f, 1.f, 0.f));

	glm::vec3 direction_width_camera_to_cannon_7 = glm::normalize(glm::vec3(2.2f, 8.85f, -170.f) - glm::vec3(camera_position_x, 8.85f, camera_position_z));
	glm::vec3 direction_height_camera_to_cannon_7 = glm::normalize(glm::vec3(2.2f, 8.85f, -170.f) - glm::vec3(2.2f, camera_position_y, camera_position_z));
	glm::vec3 direction_cannon_7 = glm::normalize(glm::vec3(2.2f, 8.85f, -170.f) - glm::vec3(2.2f, 8.85f, 0.f));
	float theta_width_cannon_7 = glm::acos(glm::dot(direction_width_camera_to_cannon_7, direction_cannon_7));
	float theta_height_cannon_7 = glm::acos(glm::dot(direction_height_camera_to_cannon_7, direction_cannon_7));
	if (camera_position_y > 8.85f) {
		theta_height_cannon_7 = -theta_height_cannon_7;
	}
	cannon_7.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(2.2f, 8.85f, -170.f)) *
								glm::rotate(glm::mat4(1.f), theta_height_cannon_7, glm::vec3(1.f, 0.f, 0.f)) *
								glm::rotate(glm::mat4(1.f), theta_width_cannon_mount_5, glm::vec3(0.f, 1.f, 0.f));

	glm::vec3 direction_width_camera_to_cannon_8 = glm::normalize(glm::vec3(3.8f, 8.85f, -170.f) - glm::vec3(camera_position_x, 8.85f, camera_position_z));
	glm::vec3 direction_height_camera_to_cannon_8 = glm::normalize(glm::vec3(3.8f, 8.85f, -170.f) - glm::vec3(3.8f, camera_position_y, camera_position_z));
	glm::vec3 direction_cannon_8 = glm::normalize(glm::vec3(3.8f, 8.85f, -170.f) - glm::vec3(3.8f, 8.85f, 0.f));
	float theta_width_cannon_8 = glm::acos(glm::dot(direction_width_camera_to_cannon_8, direction_cannon_8));
	float theta_height_cannon_8 = glm::acos(glm::dot(direction_height_camera_to_cannon_8, direction_cannon_8));
	if (camera_position_y > 8.85f) {
		theta_height_cannon_8 = -theta_height_cannon_8;
	}
	cannon_8.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(3.8f, 8.85f, -170.f)) *
							    glm::rotate(glm::mat4(1.f), theta_height_cannon_8, glm::vec3(1.f, 0.f, 0.f)) *
								glm::rotate(glm::mat4(1.f), theta_width_cannon_mount_5, glm::vec3(0.f, 1.f, 0.f));

	
	if (last_position_wall + 2.f * dt > 3.f && !check_if_walls_opening) {
		last_position_wall = 3.f;
		check_if_walls_opening = true;
	} else if (last_position_wall - 2.f * dt < 0.f && check_if_walls_opening) {
		last_position_wall = 0.f;
		check_if_walls_opening = false;
	} else {
		if (check_if_walls_opening) {
			last_position_wall -= 2.f * dt;
		}
		else {
			last_position_wall += 2.f * dt;
		}
	}

	std::cout << " Last Position: " << last_position_wall << " " << std::endl;
	wall_8.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-11.f + last_position_wall, 10.f, -130.f));
	wall_9.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f - last_position_wall, 10.f, -130.f));


	wall_11.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 1.f - last_position_wall, -160.f)) *
						       glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f));
	wall_12.app_model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -11.f + last_position_wall, -160.f)) *
						       glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f));*/




}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



bool Renderer::ReloadShaders()
{
	m_geometry_program.ReloadProgram();
	m_post_program.ReloadProgram();
	m_deferred_program.ReloadProgram();
	m_spot_light_shadow_map_program.ReloadProgram();
	return true;
}

void Renderer::Render()
{

	RenderShadowMaps();
	RenderGeometry();
	RenderDeferredShading();
	RenderPostProcess();

	GLenum error = Tools::CheckGLError();

	if (error != GL_NO_ERROR)
	{
		printf("Reanderer:Draw GL Error\n");
		system("pause");
	}
}

void Renderer::RenderPostProcess()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//glClearColor(0.f, 0.8f, 1.f, 1.f);
	glClearColor(0.1f, 0.1f, 0.1f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);

	m_post_program.Bind();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_fbo_texture);
	m_post_program.loadInt("uniform_texture", 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_light.GetShadowMapDepthTexture());
	m_post_program.loadInt("uniform_shadow_map", 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_fbo_pos_texture);
	m_post_program.loadInt("uniform_tex_pos", 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_fbo_normal_texture);
	m_post_program.loadInt("uniform_tex_normal", 3);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_fbo_albedo_texture);
	m_post_program.loadInt("uniform_tex_albedo", 4);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, m_fbo_mask_texture);
	m_post_program.loadInt("uniform_tex_mask", 5);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, m_fbo_depth_texture);
	m_post_program.loadInt("uniform_tex_depth", 6);

	glBindVertexArray(m_vao_fbo);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	m_post_program.Unbind();
}

void Renderer::RenderStaticGeometry()
{
	glm::mat4 proj = m_projection_matrix * m_view_matrix * m_world_matrix;

	for (auto& node : this->m_nodes)
	{
		glBindVertexArray(node->m_vao);

		m_geometry_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);
		m_geometry_program.loadMat4("uniform_normal_matrix", glm::transpose(glm::inverse(m_world_matrix * node->app_model_matrix)));
		m_geometry_program.loadMat4("uniform_world_matrix", m_world_matrix * node->app_model_matrix);

		for (int j = 0; j < node->parts.size(); ++j)
		{
			m_geometry_program.loadVec3("uniform_diffuse", node->parts[j].diffuse);
			m_geometry_program.loadVec3("uniform_ambient", node->parts[j].ambient);
			m_geometry_program.loadVec3("uniform_specular", node->parts[j].specular);
			m_geometry_program.loadFloat("uniform_shininess", node->parts[j].shininess);
			m_geometry_program.loadInt("uniform_has_tex_diffuse", (node->parts[j].diffuse_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_emissive", (node->parts[j].emissive_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_mask", (node->parts[j].mask_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_normal", (node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_is_tex_bumb", (node->parts[j].bump_textureID > 0) ? 1 : 0);

			glActiveTexture(GL_TEXTURE0);
			m_geometry_program.loadInt("uniform_tex_diffuse", 0);
			glBindTexture(GL_TEXTURE_2D, node->parts[j].diffuse_textureID);

			if (node->parts[j].mask_textureID > 0)
			{
				glActiveTexture(GL_TEXTURE1);
				m_geometry_program.loadInt("uniform_tex_mask", 1);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].mask_textureID);
			}

			if ((node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0))
			{
				glActiveTexture(GL_TEXTURE2);
				m_geometry_program.loadInt("uniform_tex_normal", 2);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].bump_textureID > 0 ?
					node->parts[j].bump_textureID : node->parts[j].normal_textureID);
			}

			if (node->parts[j].emissive_textureID > 0)
			{
				glActiveTexture(GL_TEXTURE3);
				m_geometry_program.loadInt("uniform_tex_emissive", 3);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].emissive_textureID);
			}

			glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
		}

		glBindVertexArray(0);
	}
}

void Renderer::RenderCollidableGeometry()
{
	glm::mat4 proj = m_projection_matrix * m_view_matrix * m_world_matrix;

	glm::vec3 camera_dir = normalize(m_camera_target_position - m_camera_position);

	for (auto& node : this->m_collidables_nodes)
	{
		float_t isectT = 0.f;
		int32_t primID = -1;
		int32_t totalRenderedPrims = 0;

		//if (node->intersectRay(m_camera_position, camera_dir, m_world_matrix, isectT, primID)) continue;
		//node->intersectRay(m_camera_position, camera_dir, m_world_matrix, isectT, primID);

		glBindVertexArray(node->m_vao);

		m_geometry_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);
		m_geometry_program.loadMat4("uniform_normal_matrix", glm::transpose(glm::inverse(m_world_matrix * node->app_model_matrix)));
		m_geometry_program.loadMat4("uniform_world_matrix", m_world_matrix * node->app_model_matrix);
		m_geometry_program.loadFloat("uniform_time", m_continous_time);

		for (int j = 0; j < node->parts.size(); ++j) {
			m_geometry_program.loadVec3("uniform_diffuse", node->parts[j].diffuse);
			m_geometry_program.loadVec3("uniform_ambient", node->parts[j].ambient);
			m_geometry_program.loadVec3("uniform_specular", node->parts[j].specular);
			m_geometry_program.loadFloat("uniform_shininess", node->parts[j].shininess);
			m_geometry_program.loadInt("uniform_has_tex_diffuse", (node->parts[j].diffuse_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_mask", (node->parts[j].mask_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_emissive", (node->parts[j].emissive_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_normal", (node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_is_tex_bumb", (node->parts[j].bump_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_prim_id", primID - totalRenderedPrims);

			glActiveTexture(GL_TEXTURE0);
			m_geometry_program.loadInt("uniform_tex_diffuse", 0);
			glBindTexture(GL_TEXTURE_2D, node->parts[j].diffuse_textureID);

			if (node->parts[j].mask_textureID > 0) {
				glActiveTexture(GL_TEXTURE1);
				m_geometry_program.loadInt("uniform_tex_mask", 1);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].mask_textureID);
			}

			if ((node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0)) {
				glActiveTexture(GL_TEXTURE2);
				m_geometry_program.loadInt("uniform_tex_normal", 2);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].bump_textureID > 0 ?
					node->parts[j].bump_textureID : node->parts[j].normal_textureID);
			}

			if (node->parts[j].emissive_textureID > 0) {
				glActiveTexture(GL_TEXTURE3);
				m_geometry_program.loadInt("uniform_tex_emissive", 3);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].emissive_textureID);
			}

			glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
			totalRenderedPrims += node->parts[j].count;
		}

		glBindVertexArray(0);
	}
}

void Renderer::RenderDeferredShading()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbo_texture, 0);

	GLenum drawbuffers[1] = { GL_COLOR_ATTACHMENT0 };

	glDrawBuffers(1, drawbuffers);

	glViewport(0, 0, m_screen_width, m_screen_height);

	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	
	glClear(GL_COLOR_BUFFER_BIT);

	m_deferred_program.Bind();

	m_deferred_program.loadVec3("uniform_light_color", m_light.GetColor());
	m_deferred_program.loadVec3("uniform_light_dir", m_light.GetDirection());
	m_deferred_program.loadVec3("uniform_light_pos", m_light.GetPosition());

	m_deferred_program.loadFloat("uniform_light_umbra", m_light.GetUmbra());
	m_deferred_program.loadFloat("uniform_light_penumbra", m_light.GetPenumbra());

	m_deferred_program.loadVec3("uniform_camera_pos", m_camera_position);
	m_deferred_program.loadVec3("uniform_camera_dir", normalize(m_camera_target_position - m_camera_position));

	m_deferred_program.loadMat4("uniform_light_projection_view", m_light.GetProjectionMatrix() * m_light.GetViewMatrix());
	m_deferred_program.loadInt("uniform_cast_shadows", m_light.GetCastShadowsStatus() ? 1 : 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_fbo_pos_texture);
	m_deferred_program.loadInt("uniform_tex_pos", 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_fbo_normal_texture);
	m_deferred_program.loadInt("uniform_tex_normal", 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_fbo_albedo_texture);
	m_deferred_program.loadInt("uniform_tex_albedo", 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_fbo_mask_texture);
	m_deferred_program.loadInt("uniform_tex_mask", 3);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_fbo_depth_texture);
	m_deferred_program.loadInt("uniform_tex_depth", 4);

	glActiveTexture(GL_TEXTURE10);
	glBindTexture(GL_TEXTURE_2D, m_light.GetShadowMapDepthTexture());
	m_deferred_program.loadInt("uniform_shadow_map", 10);

	glBindVertexArray(m_vao_fbo);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);

	m_deferred_program.Unbind();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDepthMask(GL_TRUE);
}

void Renderer::RenderGeometry()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbo_pos_texture, 0);

	GLenum drawbuffers[4] = {
		GL_COLOR_ATTACHMENT0,
		GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2,
		GL_COLOR_ATTACHMENT3 };

	glDrawBuffers(4, drawbuffers);

	glViewport(0, 0, m_screen_width, m_screen_height);
	//glClearColor(0.f, 0.8f, 1.f, 1.f);
	glClearColor(0.1f, 0.1f, 0.1f, 1.f);
	glClearDepth(1.f);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_geometry_program.Bind();
	RenderStaticGeometry();
	RenderCollidableGeometry();

	m_geometry_program.Unbind();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
}

void Renderer::RenderShadowMaps()
{
	if (m_light.GetCastShadowsStatus())
	{
		int m_depth_texture_resolution = m_light.GetShadowMapResolution();

		glBindFramebuffer(GL_FRAMEBUFFER, m_light.GetShadowMapFBO());
		glViewport(0, 0, m_depth_texture_resolution, m_depth_texture_resolution);
		glEnable(GL_DEPTH_TEST);
		glClear(GL_DEPTH_BUFFER_BIT);

		// Bind the shadow mapping program
		m_spot_light_shadow_map_program.Bind();

		glm::mat4 proj = m_light.GetProjectionMatrix() * m_light.GetViewMatrix() * m_world_matrix;

		for (auto& node : this->m_nodes)
		{
			glBindVertexArray(node->m_vao);

			m_spot_light_shadow_map_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);

			for (int j = 0; j < node->parts.size(); ++j)
			{
				glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
			}

			glBindVertexArray(0);
		}

		glm::vec3 camera_dir = normalize(m_camera_target_position - m_camera_position);
		float_t isectT = 0.f;
		int32_t primID;

		for (auto& node : this->m_collidables_nodes)
		{
			//if (node->intersectRay(m_camera_position, camera_dir, m_world_matrix, isectT, primID)) continue;
			node->intersectRay(m_camera_position, camera_dir, m_world_matrix, isectT, primID);

			glBindVertexArray(node->m_vao);

			m_spot_light_shadow_map_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);

			for (int j = 0; j < node->parts.size(); ++j)
			{
				glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
			}

			glBindVertexArray(0);
		}

		m_spot_light_shadow_map_program.Unbind();
		glDisable(GL_DEPTH_TEST);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

void Renderer::CameraMoveForward(bool enable)
{
	m_camera_movement.x = (enable) ? 1 : 0;
}

void Renderer::CameraMoveFastForward(bool enable)
{
	m_camera_movement.x = (enable) ? 4 : 0;
}

void Renderer::CameraMoveBackWard(bool enable)
{
	m_camera_movement.x = (enable) ? -1 : 0;
}

void Renderer::CameraMoveLeft(bool enable)
{
	m_camera_movement.y = (enable) ? -1 : 0;
}
void Renderer::CameraMoveRight(bool enable)
{
	m_camera_movement.y = (enable) ? 1 : 0;
}

void Renderer::CameraMoveUp(bool enable) {
	m_camera_movement.z = (enable) ? 1 : 0;
}

void Renderer::CameraMoveDown(bool enable) {
	m_camera_movement.z = (enable) ? -1 : 0;
}

void Renderer::CameraLook(glm::vec2 lookDir)
{
	m_camera_look_angle_destination = lookDir;
}