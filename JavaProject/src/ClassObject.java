
import java.util.ArrayList;

public abstract class ClassObject {

    private ArrayList<Translation> translations;
    private ArrayList<Rotation> rotations;

    public ClassObject(){
        translations = new ArrayList<Translation>();
        rotations = new ArrayList<Rotation>();
    }

    public void addTranslation(Translation translation){
        translations.add(translation);
    }

    public void addRotation(Rotation rotation){
        rotations.add(rotation);
    }

}
